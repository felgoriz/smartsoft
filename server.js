var mongoose = require('./config/mongoose'),
  express = require('./config/express');

var dbUri = 'mongodb://localhost/smartsoft';
var appHost = 'localhost';
var appPort = 8080;

var db = mongoose(dbUri);
var app = express();
app.listen(appPort, appHost);

console.log('Server running at http://' + appHost + ':' + appPort + '/');