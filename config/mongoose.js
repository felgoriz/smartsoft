var mongoose = require('mongoose');

module.exports = function(dbUri) {
  var db = mongoose.connect(dbUri);
  require('../app/models/rule.server.model');
  return db;
};