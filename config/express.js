var express = require('express'),
  bodyParser = require('body-parser');

module.exports = function() {
  var app = express();
  app.use(bodyParser.json());
  require('../app/routes/api.server.routes.js')(app);
  return app;
};