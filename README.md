# INSTRUCCIONES #


## Repositorio ##

https://bitbucket.org/felgoriz/smartsoft


## Configuración ##

Modificar en el archivo “server.js” ubicado en la raíz del repositorio los valores necesarios para uri de la base de datos en mongo, el host y el puerto de la aplicación.

* var dbUri = 'mongodb://localhost/smartsoft';
* var appHost = 'localhost';
* var appPort = 8080;


## Ejecución ##

Ejecutar los siguientes comandos en una consola:

1. npm install
2. node server.js


## Pruebas ##

Para las pruebas se usó la herramienta curl para hacer las peticiones al api creado. Los siguientes son los comandos usados:


### Crear o actualizar una regla (se crearon 3 reglas de prueba): ###

* curl -XPOST -H "Content-type: application/json" -d '{"name":"rule1","condition":{"type":"and","inputs":[{"type":"compare","a":{"type":"fact","field":"Monto"},"condition":">","b":{"type":"constant","value":1400}},{"type":"compare","a":{"type":"fact","field":"Comercio"},"condition":"==","b":{"type":"constant","svalue":"Mac"}}]}}' 'localhost:8080/api/rule/saveOrUpdate'
* curl -XPOST -H "Content-type: application/json" -d '{"name":"rule2","condition":{"type":"or","inputs":[{"type":"and","inputs":[{"type":"compare","a":{"type":"fact","field":"Comercio"},"condition":"==","b":{"type":"constant","svalue":"POPs"}},{"type":"compare","a":{"type":"fact","field":"Hora"},"condition":">","b":{"type":"constant","value":10}}]},{"type":"compare","a":{"type":"fact","field":"Monto"},"condition":"<","b":{"type":"constant","value":500}}]}}' 'localhost:8080/api/rule/saveOrUpdate'
* curl -XPOST -H "Content-type: application/json" -d '{"name":"rule3","condition":{"type":"compare","a":{"type":"fact","field":"Hora"},"condition":"<","b":{"type":"constant","value":28}}}' 'localhost:8080/api/rule/saveOrUpdate'

### Listar reglas: ###

curl -XGET -H "Content-type: application/json" 'localhost:8080/api/rule/list'

### Obtener regla: ###

curl -XGET -H "Content-type: application/json" 'localhost:8080/api/rule/{idRule}/find'

### Validar hecho: ###

curl -XPOST -H "Content-type: application/json" -d '{"Monto":1500,"Comercio":"Mac","Hora":17}' 'localhost:8080/api/fact/validate'