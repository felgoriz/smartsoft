var shortid = require('shortid'),
  mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var RuleSchema = new Schema({
  idRule: {
    type: String,
    index: {unique: true},
    required: 'The idRule is required',
    default: shortid.generate
  },
  name: {
    type: String,
    required: 'The name is required'
  },
  condition: {
    type: Object,
    required: 'The condition is required'
  }
});

RuleSchema.statics.saveOrUpdate = function(rule, callback) {
  var conditions = {idRule: rule.idRule};
  var update = {idRule: rule.idRule, name: rule.name, condition: rule.condition};
  var options = {upsert: true, setDefaultsOnInsert: true};
  this.findOneAndUpdate(conditions, update, options, callback);
};

RuleSchema.statics.list = function(callback) {
  var conditions = null;
  var projection = '-_id idRule name';
  var options = null;
  this.find(conditions, projection, options, callback);
};

RuleSchema.statics.findByIdRule = function(idRule, callback) {
  var conditions = {idRule: idRule};
  var projection = '-_id idRule condition';
  this.find(conditions, projection, callback);
};

mongoose.model('Rule', RuleSchema);