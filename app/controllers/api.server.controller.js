var ruleValidator = require('../../app/utils/rule.server.validator'),
  factValidator = require('../../app/utils/fact.server.validator'),
  Rule = require('mongoose').model('Rule');

exports.validateFact = function(req, res, next) {
  var fact = req.body;
  Rule.find({}, function (err, rules) {
    if (err) {
      return next(err);
    }
    res.json(factValidator.validateRules(fact, rules));
  });
};

exports.saveOrUpdateRule = function(req, res, next) {
  var rule = new Rule(req.body);
  var validationResult = ruleValidator.validate(rule.condition);
  if (validationResult.isValid === false) {
    res.status(400).send(validationResult.message);
  } else {
    Rule.saveOrUpdate(rule, function(err) {
      if (err) {
        return next(err);
      }
      res.json(rule);
    });
  }
};

exports.listRules = function(req, res, next) {
  Rule.list(function(err, rules) {
    if (err) {
      return next(err);
    }
    res.json(rules);
  });
};

exports.findRule = function(req, res, next) {
  var idRule = req.params.idRule;
  Rule.findByIdRule(idRule, function(err, rule) {
    if (err) {
      return next(err);
    }
    res.json(rule);
  });
};

exports.listRulesFull = function(req, res, next) {
  Rule.list(function(err, rules) {
    if (err) {
      return next(err);
    }
    res.json(rules);
  });
};
