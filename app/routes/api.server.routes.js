var api = require('../../app/controllers/api.server.controller');

module.exports = function(app) {
  app.route('/api/rule/saveOrUpdate').post(api.saveOrUpdateRule);
  app.route('/api/rule/list').get(api.listRules);
  app.route('/api/rule/:idRule/find').get(api.findRule);
  app.route('/api/fact/validate').post(api.validateFact);
};