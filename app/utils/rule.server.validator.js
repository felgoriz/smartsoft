var validateLogicOperation = function(object) {
  var type = object.type;
  switch (type) {
    case 'and':
    case 'or':
      return validateAndOr(object);
    case 'compare':
      return validateCompare(object);
    case undefined:
    default:
      return {isValid: false, message: type + ' is not a valid type, should be [and|or|compare]'
      };
  }
};

var validateValueOrArithmeticOperation = function(object) {
  var type = object.type;
  switch (type) {
    case 'fact':
      return validateFact(object);
    case 'add':
    case 'mul':
      return validateAddMul(object);
    case 'sub':
    case 'div':
      return validateSubDiv(object);
    case 'constant':
      return validateConstant(object);
    case undefined:
    default:
      return {isValid: false, message: type
          + ' is not a valid type, should be [fact|add|mul|sub|div|constant]'};
  }
};

var validateFact = function(object) {
  var field = object.field;
  if (typeof field !== 'string') {
    return {isValid: false, message: field + ' is not a valid field, should be a String'};
  }
  return {isValid: true};
};

var validateAndOr = function(object) {
  var inputs = object.inputs;
  if (!(inputs instanceof Array)) {
    return {isValid: false, message: inputs + ' are not valid inputs, should be an Array of Object'
    };
  }
  return validateLogicOperationArray(inputs);
};

var validateCompare = function(object) {
  var condition = object.condition;
  if (typeof condition !== 'string') {
    return {isValid: false, message: condition + ' is not a valid condition, should be a String'};
  }
  var a = object.a;
  if (typeof a !== 'object') {
    return {isValid: false, message: a + ' is not a valid operator, should be an Object'};
  }
  var b = object.b;
  if (typeof b !== 'object') {
    return {isValid: false, message: b + ' is not a valid operator, should be an Object'};
  }
  var conditionValidationResult = validateCondition(condition);
  if (!conditionValidationResult.isValid) {
    return conditionValidationResult;
  }
  var aValidationResult = validateValueOrArithmeticOperation(a);
  if (!aValidationResult.isValid) {
    return aValidationResult;
  }
  var bValidationResult = validateValueOrArithmeticOperation(b);
  if (!bValidationResult.isValid) {
    return bValidationResult;
  }
  return {isValid: true};
};

var validateAddMul = function(object) {
  var inputs = object.inputs;
  if (!(inputs instanceof Array)) {
    return {isValid: false, message: inputs + ' are not valid inputs, should be an Array of Object'
    };
  }
  return validateValueOrArithmeticOperationArray(inputs);
};

var validateSubDiv = function(object) {
  var a = object.a;
  if (typeof a !== 'object') {
    return {isValid: false, message: a + ' is not a valid operator, should be an Object'};
  }
  var b = object.b;
  if (typeof b !== 'object') {
    return {isValid: false, message: b + ' is not a valid operator, should be an Object'};
  }
  var aValidationResult = validateValueOrArithmeticOperation(a);
  if (!aValidationResult.isValid) {
    return aValidationResult;
  }
  var bValidationResult = validateValueOrArithmeticOperation(b);
  if (!bValidationResult.isValid) {
    return bValidationResult;
  }
  return {isValid: true};
};

var validateConstant = function(object) {
  var value = object.value;
  if (typeof value !== 'number') {
    var svalue = object.svalue;
    if (typeof svalue !== 'string') {
      return {isValid: false, message: value + ' is invalid value and ' + svalue
          + ' is invalid svalue, should be a Number or a String'};
    }
  }
  return {isValid: true};
};

var validateLogicOperationArray = function(array) {
  for (var i = 0; i < array.length; i++) {
    var object = array[i];
    var validationResult = validateLogicOperation(object);
    if (!validationResult.isValid) {
      return validationResult;
    }
  }
  return {isValid: true};
};

var validateValueOrArithmeticOperationArray = function(array) {
  for (var i = 0; i < array.length; i++) {
    var object = array[i];
    var validationResult = validateValueOrArithmeticOperation(object);
    if (!validationResult.isValid) {
      return validationResult;
    }
  }
  return {isValid: true};
};

var validateCondition = function(condition) {
  if (!condition.match(/^(<|<=|==|!=|>=|>|like|!like|regex|!regex)$/)) {
    return {isValid: false, message: condition
        + ' is not a valid condition, should be [<|<=|==|!=|>=|>|like|!like|regex|!regex]'};
  }
  return {isValid: true};
};

exports.validate = function(condition) {
  if (condition === undefined) {
    return {isValid: false, message: condition  + ' is not a valid condition'};
  }  
  return validateLogicOperation(condition);
};