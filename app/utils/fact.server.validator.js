var processLogicOperation = function(object) {
    var type = object.type;
    switch(type) {
        case 'and':
        case 'or':
            return processAndOr(object);
        case 'compare':
            return processCompare(object);
        case undefined:
        default:        
            return '';            
    }
};

var processValueOrArithmeticOperation = function(object) {
    var type = object.type;
    switch(type) {
        case 'fact':
            return processFact(object);
        case 'add':
        case 'mul':            
            return processAddMul(object);
        case 'sub':
        case 'div':
            return processSubDiv(object);
        case 'constant':
            return processConstant(object);
        case undefined:
        default:        
            return '';            
    }
};

var processFact = function(object) {
    var field = object.field;
    return 'fact["' + field + '"]';
};

var processAndOr = function(object) {
    var type = object.type;
    var inputs = object.inputs;
    return processLogicOperationArray(type, inputs);
};

var processCompare = function(object) {
    var condition = object.condition;
    var a = object.a;
    var b = object.b;
    var outputArray = [];
    outputArray.push(processValueOrArithmeticOperation(a));
    outputArray.push(processValueOrArithmeticOperation(b));
    return "(" + outputArray.join(' ' + condition + ' ') + ")";
};

var processAddMul = function(object) {
    var type = object.type;    
    var inputs = object.inputs;
    return processValueOrArithmeticOperationArray(type, inputs);
};

var processSubDiv = function(object) {
    var operations = {"sub": " - ", "or": " || "};
    var type = object.type;    
    var a = object.a;
    var b = object.b;
    var outputArray = [];
    outputArray.push(processValueOrArithmeticOperation(a));
    outputArray.push(processValueOrArithmeticOperation(b));
    return "(" + outputArray.join(' ' + operations[type] + ' ') + ")";
};

var processConstant = function(object) {
    var value = object.value;
    if (value == undefined) {
        value = '"' + object.svalue + '"';
    }
    return value;
}; 

var processLogicOperationArray = function(type, array) {
    var operations = {"and": " && ", "or": " || "};
    var outputArray = [];
    for (var i = 0; i < array.length; i++) {
        var object = array[i];
        outputArray.push(processLogicOperation(object));
    }
     return "(" + outputArray.join(operations[type]) + ")";
};

var processValueOrArithmeticOperationArray = function(type, array) {
    var operations = {"add": " + ", "mul": " * "};
    var outputArray = [];
    for (var i = 0; i < array.length; i++) {
        var object = array[i];
        outputArray.push(processValueOrArithmeticOperation(object));
    }
    return "(" + outputArray.join(operations[type]) + ")";
};

var convertRules = function(rules) {
    var convertedRules = [];
    for (var i = 0; i < rules.length; i++) {
        var convertedRule = processLogicOperation(rules[i].condition);
        convertedRules.push(convertedRule);
    }
    return convertedRules;
};

exports.validateRules = function(fact, rules) {
    var convertedRules = convertRules(rules);
    var conditions = {};
    var consequence = [];
    for (var i = 0; i < convertedRules.length; i++) {
        var convertedRule = convertedRules[i];
        var documentId = i + 1;
        conditions[documentId] = convertedRule;
        if (eval(convertedRule)) {
            consequence.push(documentId);
        }
    }
    console.dir(conditions);
    return consequence;
};